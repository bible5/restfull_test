package com.example.seong_gyeong_o.sendbirdtestapp.network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class VolleyNetworkModule extends AsyncTask<Void, Void, Void> implements SharedNetwork {

    Context mContext;
    NetworkCallback mNetworkCallback;

    public VolleyNetworkModule(Context context, NetworkCallback networkCallback) {
        mContext = context;
        mNetworkCallback = networkCallback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("user_id", "tjdruddh");
            jsonObject.accumulate("nickname", "bible5");
            jsonObject.accumulate("profile_url", "https://dddd/eeee/ffff");
            jsonObject.accumulate("issue_access_token", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ROOT_URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response Data", response.toString());
                mNetworkCallback.success(response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Erorr Data", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<String, String>();
                header.put("Content-Type", "application/json, charset=utf8");
                header.put("Api-Token", API_TOKEN);

                return header;
            }
        };

        Volley.newRequestQueue(mContext).add(jsonObjectRequest);

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

}
