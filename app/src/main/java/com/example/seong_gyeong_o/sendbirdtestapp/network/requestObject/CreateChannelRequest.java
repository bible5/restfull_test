package com.example.seong_gyeong_o.sendbirdtestapp.network.requestObject;

import java.io.File;
import java.util.List;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class CreateChannelRequest {
    String name;
    String channelUrl;
    File coverFile;
    String customType;
    String data;
    List<String> operators;

    public CreateChannelRequest(String name, String channelUrl, File coverFile, String customType, String data,
                                List<String> operators) {
        this.name = name;
        this.channelUrl = channelUrl;
        this.coverFile = coverFile;
        this.customType = customType;
        this.data = data;
        this.operators = operators;
    }

    public String getName() {
        return name;
    }

    public String getChannelUrl() {
        return channelUrl;
    }

    public File getCoverFile() {
        return coverFile;
    }

    public String getCustomType() {
        return customType;
    }

    public String getData() {
        return data;
    }

    public List<String> getOperators() {
        return operators;
    }
}
