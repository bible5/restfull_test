package com.example.seong_gyeong_o.sendbirdtestapp.network.responseObject;

import com.example.seong_gyeong_o.sendbirdtestapp.network.Users;
import com.example.seong_gyeong_o.sendbirdtestapp.network.requestObject.ListUserRequest;

import java.util.List;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class ListUserResponse {

    List<Users> userList;
    String next;

    public ListUserResponse(List<Users> userList, String next) {
        this.userList = userList;
        this.next = next;
    }

    public List<Users> getUserList() {
        return userList;
    }

    public String getNext() {
        return next;
    }
}
