package com.example.seong_gyeong_o.sendbirdtestapp.network.responseObject;

import java.io.File;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class CreateUserResponse {
    String userId;
    String nickName;
    File profileFile;
    boolean issueAccessToken;

    public CreateUserResponse(String userId, String nickName, File profileFile, boolean issueAccessToken) {
        this.userId = userId;
        this.nickName = nickName;
        this.profileFile = profileFile;
        this.issueAccessToken = issueAccessToken;
    }

    public String getUserId() {
        return userId;
    }

    public String getNickName() {
        return nickName;
    }

    public File getProfileFile() {
        return profileFile;
    }

    public boolean getIssueAccessToken() {
        return issueAccessToken;
    }
}
