package com.example.seong_gyeong_o.sendbirdtestapp.activity;

import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.seong_gyeong_o.sendbirdtestapp.R;
import com.example.seong_gyeong_o.sendbirdtestapp.network.HttpURLConnectionNetworkModule;
import com.example.seong_gyeong_o.sendbirdtestapp.network.SharedNetwork;
import com.example.seong_gyeong_o.sendbirdtestapp.network.VolleyNetworkModule;

public class MainActivity extends AppCompatActivity {

    TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView)findViewById(R.id.selectedUser_tv);

        SharedNetwork.NetworkCallback networkCallback = new SharedNetwork.NetworkCallback() {
            @Override
            public void success(Object object) {
                mTextView.setText((String)object);
            }

            @Override
            public void error(Object object) {

            }
        };

//        HttpURLConnectionNetworkModule httpClientNetworkModule = new HttpURLConnectionNetworkModule(networkCallback);
//        httpClientNetworkModule.execute();

        VolleyNetworkModule volleyNetworkModule = new VolleyNetworkModule(getApplicationContext(), networkCallback);
        volleyNetworkModule.execute();

    }
}
