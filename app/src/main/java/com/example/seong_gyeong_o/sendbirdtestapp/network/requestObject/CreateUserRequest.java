package com.example.seong_gyeong_o.sendbirdtestapp.network.requestObject;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class CreateUserRequest {
    String userId;
    String nickName;
    String profileUrl;
    boolean issueAccessToken;

    public CreateUserRequest(String userId, String nickName, String profileUrl, boolean issueAccessToken) {
        this.userId = userId;
        this.nickName = nickName;
        this.profileUrl = profileUrl;
        this.issueAccessToken = issueAccessToken;
    }

    public String getUserId() {
        return userId;
    }

    public String getNickName() {
        return nickName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public boolean getIssueAccessToken() {
        return issueAccessToken;
    }
}
