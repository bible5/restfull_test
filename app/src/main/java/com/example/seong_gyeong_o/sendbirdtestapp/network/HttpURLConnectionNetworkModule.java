package com.example.seong_gyeong_o.sendbirdtestapp.network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.seong_gyeong_o.sendbirdtestapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ContentHandler;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class HttpURLConnectionNetworkModule extends AsyncTask<Void, Void, Void> implements SharedNetwork{

    String data = null;
    NetworkCallback mNetworkCallback = null;


    public HttpURLConnectionNetworkModule(NetworkCallback networkCallback) {
        mNetworkCallback = networkCallback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Void doInBackground(Void... voids) {

        HttpURLConnection httpURLConnection = null;

        OutputStream outputStream = null;
        BufferedReader bufferedReader = null;
        StringBuilder stringBuilder = null;
        String line = null;

        try {
            URL url = new URL(ROOT_URL);

            httpURLConnection = (HttpURLConnection)url.openConnection();

            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json, charset=utf8");
            httpURLConnection.setRequestProperty("Api-Token", API_TOKEN);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);

            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("user_id", "성경오3");
            jsonObject.accumulate("nickname", "hahahaha3");
            jsonObject.accumulate("profile_url", "https://dddd/eeee/ffff");
            jsonObject.accumulate("issue_access_token", true);

            outputStream = httpURLConnection.getOutputStream();
            outputStream.write(jsonObject.toString().getBytes());
            outputStream.flush();

            int resCode = httpURLConnection.getResponseCode();

            if(SUCCESS_RESPONSE == resCode) {
                bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                stringBuilder = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + '\n');
                }

                data = stringBuilder.toString();
                Log.d("ResPonse Data :", stringBuilder.toString());
            } else {
                bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                stringBuilder = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + '\n');
                }
                Log.d("error Message", stringBuilder.toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        mNetworkCallback.success(data);

    }
}
