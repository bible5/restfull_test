package com.example.seong_gyeong_o.sendbirdtestapp.network;

import android.graphics.Path;

import com.example.seong_gyeong_o.sendbirdtestapp.network.responseObject.ListUserResponse;

import java.io.File;
import java.util.List;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class OpenChannel {
    String name;
    String channelUrl;
    String coverUrl;
    File coverFile;
    String data;
    List<Users> operators;
    String customType;
    List<String> participants;
    int participantCount;
    int maxLengthMessage;
    long createdAt;
    boolean freeze;

    public OpenChannel(String name, String channelUrl, String coverUrl, File coverFile, String data,
                       List<Users> operators, String customType, List<String> participants, int participantCount,
                       int maxLengthMessage, long createdAt, boolean freeze) {
    }

    public String getName() {
        return name;
    }

    public String getChannelUrl() {
        return channelUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public File getCoverFile() {
        return coverFile;
    }

    public String getData() {
        return data;
    }

    public List<Users> getOperators() {
        return operators;
    }

    public String getCustomType() {
        return customType;
    }

    public List<String> getParticipants() {
        return participants;
    }

    public int getParticipantCount() {
        return participantCount;
    }

    public int getMaxLengthMessage() {
        return maxLengthMessage;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public boolean getFreeze() {
        return freeze;
    }

}
