package com.example.seong_gyeong_o.sendbirdtestapp.network.responseObject;

import android.icu.lang.UScript;

import com.example.seong_gyeong_o.sendbirdtestapp.network.Users;
import com.example.seong_gyeong_o.sendbirdtestapp.network.requestObject.UpdateUserRequest;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class UpdateUserResponse {

    Users users;

    public UpdateUserResponse(Users users) {
        this.users = users;
    }

    public Users getUsers() {
        return users;
    }
}
