package com.example.seong_gyeong_o.sendbirdtestapp.network.requestObject;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class ListUserRequest {
    String token;
    int limit;
    String activeMode;
    boolean showBot;
    String userIds;

    public ListUserRequest(String token, int limit, String activeMode, boolean showBot, String userIds) {
        this.token = token;
        this.limit = limit;
        this.activeMode = activeMode;
        this.showBot = showBot;
        this.userIds = userIds;
    }

    public String getToken() {
        return token;
    }

    public int getLimit() {
        return limit;
    }

    public String getActiveMode() {
        return activeMode;
    }

    public boolean getShowBot() {
        return showBot;
    }

    public String getUserIds() {
        return userIds;
    }
}
