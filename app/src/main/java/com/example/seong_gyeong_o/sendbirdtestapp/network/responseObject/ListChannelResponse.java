package com.example.seong_gyeong_o.sendbirdtestapp.network.responseObject;

import com.example.seong_gyeong_o.sendbirdtestapp.network.OpenChannel;
import com.example.seong_gyeong_o.sendbirdtestapp.network.requestObject.ListMessageRequest;

import java.util.List;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class ListChannelResponse {
    List<OpenChannel> channels;

    public ListChannelResponse(List<OpenChannel> channels) {
        this.channels = channels;
    }

    public List<OpenChannel> getChannels() {
        return channels;
    }
}
