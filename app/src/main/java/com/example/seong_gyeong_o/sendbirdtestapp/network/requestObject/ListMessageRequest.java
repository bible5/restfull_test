package com.example.seong_gyeong_o.sendbirdtestapp.network.requestObject;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class ListMessageRequest {
    long messageTs;
    int prevLimit;
    int nextLimit;
    boolean include;
    boolean reverse;
    String customType;
    String messageType;
    String senderId;

    public ListMessageRequest(long messageTs, int prevLimit, int nextLimit,
                              boolean include, boolean reverse, String customType,
                              String messageType, String senderId) {
        this.messageTs = messageTs;
        this.prevLimit = prevLimit;
        this.nextLimit = nextLimit;
        this.include = include;
        this.reverse = reverse;
        this.customType = customType;
        this.messageType = messageType;
        this.senderId = senderId;
    }

    public long getMessageTs() {
        return messageTs;
    }

    public int getPrevLimit() {
        return prevLimit;
    }

    public int getNextLimit() {
        return nextLimit;
    }

    public boolean getInclude() {
        return include;
    }

    public boolean getReverse() {
        return reverse;
    }

    public String getCustomType() {
        return customType;
    }

    public String getMessageType() {
        return messageType;
    }

    public String getSenderId() {
        return senderId;
    }

}
