package com.example.seong_gyeong_o.sendbirdtestapp.network;

import android.net.Uri;
import android.os.AsyncTask;

import java.net.URI;
import java.util.Objects;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public interface SharedNetwork {

    // URL String
    String ROOT_URL = "https://api.sendbird.com/v3/users";
    String API_TOKEN = "4db02df4616daf4020490f4fe7290bcbf212a3f9";
    String APP_ID = "AF941C70-CED4-46E2-863B-B1C25407F675";

    int SUCCESS_RESPONSE = 200;
    int ERORR_RESPONSE = 400;

    interface NetworkCallback {
        void success(Object object);
        void error(Object object);

    }
}
