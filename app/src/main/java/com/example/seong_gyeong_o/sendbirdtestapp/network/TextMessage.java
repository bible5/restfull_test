package com.example.seong_gyeong_o.sendbirdtestapp.network;

import java.io.File;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class TextMessage {

    String messageType;
    String userId;
    String message;
    String customType;
    String data;
    boolean markAsRead;

    public TextMessage(String messageType, String userId, String message, String customType,
                       String data, boolean markAsRead) {
        this.messageType = messageType;
        this.userId = userId;
        this.message = message;
        this.customType = customType;
        this.data = data;
        this.markAsRead = markAsRead;
    }

    public String getMessageType() {
        return messageType;
    }

    public String getUserId() {
        return userId;
    }

    public String getMessage() {
        return message;
    }

    public String getCustomType() {
        return customType;
    }

    public String getData() {
        return data;
    }

    public boolean getMarkAsRead() {
        return markAsRead;
    }
}
