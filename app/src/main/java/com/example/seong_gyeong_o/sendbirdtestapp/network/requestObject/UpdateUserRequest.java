package com.example.seong_gyeong_o.sendbirdtestapp.network.requestObject;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class UpdateUserRequest {
    String nickName;
    String profileUrl;
    boolean issueAccessToken;
    boolean isActive;
    boolean leaveAllWhenDeactivated;

    public UpdateUserRequest (String nickName, String profileUrl, boolean issueAccessToken, boolean isActive,
                              boolean leaveAllWhenDeactivated) {
        this.nickName = nickName;
        this.profileUrl = profileUrl;
        this.issueAccessToken = issueAccessToken;
        this.isActive = isActive;
        this.leaveAllWhenDeactivated = leaveAllWhenDeactivated;
    }

    public String getNickName() {
        return nickName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public boolean getIssueAccessToken() {
        return issueAccessToken;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public boolean getLeaveAllWhenDeactivated() {
        return leaveAllWhenDeactivated;
    }
}
