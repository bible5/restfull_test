package com.example.seong_gyeong_o.sendbirdtestapp.network;

import java.io.File;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class Users {
    String userId;
    String nickName;
    String profileUrl;
    File profileFile;
    String accessToken;
    boolean isActive;
    boolean isOnline;
    long lastSeenAt;

    public Users(String userId, String nickName, String profileUrl, File profileFile, String accessToken,
                 boolean isActive, boolean isOnline, long lastSeenAt) {
        this.userId = userId;
        this.nickName = nickName;
        this.profileUrl = profileUrl;
        this.profileFile = profileFile;
        this.accessToken = accessToken;
        this.isActive = isActive;
        this.isOnline = isOnline;
        this.lastSeenAt = lastSeenAt;
    }

    public String getUserId() {
        return userId;
    }

    public String getNickName() {
        return nickName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public File getProfileFile() {
        return profileFile;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public boolean getIsOnline() {
        return isOnline;
    }

    public long getLastSeenAt() {
        return lastSeenAt;
    }
}
