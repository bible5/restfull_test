package com.example.seong_gyeong_o.sendbirdtestapp.network.requestObject;

import com.example.seong_gyeong_o.sendbirdtestapp.network.responseObject.ListUserResponse;

/**
 * Created by seong-gyeong-o on 2017. 3. 27..
 */

public class ListChannelRequest {

    String token;
    int limit;
    String customType;

    public ListChannelRequest(String token, int limit, String customType) {
        this.token = token;
        this.limit = limit;
        this.customType = customType;
    }

    public String getToken() {
        return token;
    }

    public int getLimit() {
        return limit;
    }

    public String getCustomType() {
        return customType;
    }
}
